# -*- coding: utf-8 -*-
import sys
import os 
from PIL import Image

from lettuce import step
from lettuce import world


sys.path.append("../")
from geomailing.converter import AddressConverter, CSVReader
from geomailing.downloader import AddressImageDownloader



@step(u'For a given address')
def for_a_given_address(step):
    world.address = "25 rue beaurepaire 75010 paris"

@step(u'Give me the Google map URL that shows this place')
def give_me_the_google_map_url_that_shows_this_place(step):
    addressConverter = AddressConverter()
    world.url = addressConverter.convert(world.address)
    assert "http://maps.googleapis.com/maps/api/staticmap?zoom=16&size=400x400&maptype=roadmap&format=jpg&markers=size:mid%7color:blue%7label:ici%725%20rue%20beaurepaire%2075010%20paris&sensor=false" == world.url, world.url

@step(u'Download the file set at this URL')
def download_the_file_set_at_this_url(step):
    downloader = AddressImageDownloader()
    file = downloader.download(world.address, world.url)    
    assert os.path.exists(file.name)

    im = Image.open(file.name)
    assert (400, 400) == im.size

@step(u'Give an address list')
def give_an_address_list(step):
    world.csvfile = ""
    world.addresses = ["25 rue beaurepaire 75010 paris",\
            "10 rue boyer 75020 paris", "7 rue de la mare 75020 paris"]

@step(u'Download files for each of these addresses')
def download_files_for_each_of_these_addresses(step):
    downloader = AddressImageDownloader()

    if world.csvfile:
        files = downloader.downloadList(world.addresses, "addresses")
    else:
        files = downloader.downloadList(world.addresses)

    for i, file in enumerate(files):
        assert os.path.exists(file.name)
        assert str(i).zfill(5) + "_" in file.name

        im = Image.open(file.name)
        assert (400, 400) == im.size

@step(u'For a file located at ./addresses.csv')
def for_a_file_located_at_test_csv(step):
    world.csvfile = "./addresses.csv"

@step(u'Extract address list')
def extract_address_list(step):
    reader = CSVReader()
    world.addresses = reader.extract_addresses(world.csvfile)
    assert 3 == len(world.addresses)
    assert "25 rue beaurepaire 75010 Paris France" == world.addresses[0]
    assert "7 rue de la mare 75020 Paris France" == world.addresses[1]
    assert "24 rue de l'Est 75020 Paris France" == world.addresses[2]


@step(u'Check that the image folder has the same name as CSV file')
def check_that_the_image_folder_has_the_same_name_as_csv_file(step):
    assert os.path.isdir("addresses")
    for root, dirs, files in os.walk("./addresses/"):
        assert len(world.addresses) == len(files), str(len(world.addresses)) + " "+ str(len(files))

