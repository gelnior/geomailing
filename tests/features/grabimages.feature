Feature: Retrieve image list written inside CSV list


    Scenario: Convert Address to url
        For a given address
        Give me the Google map URL that shows this place

    Scenario: Download map images
        For a given address
        Give me the Google map URL that shows this place
        Download the file set at this URL 

    Scenario: Download list of map images
        Give an address list
        Download files for each of these addresses

    Scenario: Extract address from CSV file
        For a file located at ./addresses.csv
        Extract address list        

    Scenario: Download map images for a list extracted from a CSV File
        For a file located at ./addresses.csv
        Extract address list
        Download files for each of these addresses
        Check that the image folder has the same name as CSV file

         
