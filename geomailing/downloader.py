# -*- coding: utf-8 -*-
IMAGE_FOLDER = "images"

import requests
import os

from converter import AddressConverter


class AddressImageDownloader():


    def download(self, address, url, prefix="", folder=IMAGE_FOLDER):

        if not os.path.exists(folder):
            os.makedirs(folder)
        filename =  os.path.join(folder, 
                                 prefix + address.replace(" ", "-") + ".jpg")
        request = requests.get(url)
        picfile = open(filename, 'wb')
        picfile.write(request.content)
        picfile.close()

        return picfile


    def downloadList(self, addresses, folder=IMAGE_FOLDER):
        converter = AddressConverter()
        files = list()

        for i, address in enumerate(addresses):
            print "Téléchargement de l'image de l'adresse : %s" % address
            if address[0:4] != "http":
                url = converter.convert(address)
            prefix =  str(i).zfill(5) + "_"
            picfile = self.download(address, url, prefix, folder)
            files.append(picfile)
       
        return files

