import csv
import os
from exception import Usage



class AddressConverter():
    '''
    Convert postal address to Google Map URL.
    '''


    def convert(self, address):
        '''
        Convert *address* to Google Map URL.
        '''

        convertedUrl = ""
        if address:            
            baseurl = "http://maps.googleapis.com/maps/api/staticmap?" \
                     "zoom=16&size=400x400&maptype=roadmap&format=jpg" \
                     "&markers=size:mid%7color:blue%7label:ici%7"
        
            convertedUrl =  baseurl + address.replace(" ", "%20") + \
                    "&sensor=false"

        return convertedUrl
    


class CSVReader():
    '''
    Tools to check file validity and extract data from it.
    '''


    def check_file(self, fileName):
        '''
        Check that file has right extension : .csv.
        '''

        if len(fileName) <= 4 or fileName[-4:len(fileName)] != '.csv':
            raise Usage("Geomailing works only for CSV file.")


    def extract_addresses(self, csvfile):
        '''
        Extract addresses from CSV file where all address should be in
        first column.
        '''

        addresses = []
        reader = csv.reader(open(csvfile, 'rb'), delimiter=',', quotechar='"')
        for row in reader:
            addresses.append(row[0])
        return addresses


    def make_image_dir_for_file(self, fileName):
        '''
        Make dir that have same name as fileName without CSV extension.
        '''

        csvfile = open(fileName, 'r')
        dirName = "./" + os.path.basename(csvfile.name)[0:-4]
        if not os.path.exists(dirName):
            os.makedirs(dirName)

        return dirName




