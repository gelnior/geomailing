# -*- coding: utf-8 -*-

import sys
import getopt

from converter import CSVReader
from downloader import AddressImageDownloader
from exception import Usage


def check_args(args):
    if len(args) != 1:
        raise Usage("Geomailing ne fonctionne qu'avec un seul fichier " \
                    "veuillez en fournir un.")


def process(args):
    fileName = args[0]
    reader = CSVReader()
    downloader = AddressImageDownloader()
   
    reader.check_file(fileName)
    dirName = reader.make_image_dir_for_file(fileName)
    addresses = reader.extract_addresses(fileName)
    downloader.downloadList(addresses, dirName)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
            if opts:
                print """
Geomailing est un outil permettant de télécharger des cartes Google à partir 
d'une liste d'adresses postales contenues dans la première colonne d'un 
fichier CSV.
  
Utilisation:
geomailig mon_fichier.csv
                      """
            else:
                check_args(args)
                process(args)

        except getopt.error, msg:
             raise Usage(msg)

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "Pour l'aide ajouter l'option --help"
        return 2


if __name__ == "__main__":
    sys.exit(main())

