from setuptools import setup, find_packages

setup(name='Geomailing',
      version='0.2.0',
      description='An app to grab google maps easily',
      author='Frank Rousseau',
      author_email='frank.rousseau@free.fr',
      packages=find_packages(),
      install_requires=[
        'requests>=0.6.0',
      ],
      entry_points = {
        'console_scripts': [
            'geomailing = geomailing.geomailing:main',
        ]
      }
)
